import classifier

import load_headlines

from sklearn.cross_validation import train_test_split

X, y = load_headlines.load_dataset(['haaretz.csv', 'israelhayom.csv'])
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=33)

x = classifier.Classifier()
res = x.classify(X_test)

success = 0
all = 0

for i in range(len(X_test)):
    if res[i] == y_test[i]:
        success += 1
    all += 1

print('accuracy: ', success/all)
