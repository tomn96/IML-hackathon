
# tar
tar:
	tar -cvf iml_hackathon.tar babies_classifier.pkl classifier.py project.pdf README requirements.txt USERS load_headlines.py


# Other Targets
clean:
	-rm -f *.tar

