"""
===================================================
     Introduction to Machine Learning (67577)
             IML HACKATHON, June 2017

            **  Headline Classifier  **

Auther(s):

===================================================
"""

from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier
from sklearn.cross_validation import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from nltk.stem import PorterStemmer
import os.path
import pickle


TRAIN_CLASSIFIER = False  # change in order to train our classifier with the data
SAVE_PICKLE = False


def pre_process(head):
    length = '@' + str(int(len(head) / 10))

    lst = head.split()
    res = ' '.join([PorterStemmer().stem(word.lower()) for word in lst])

    return res + " " + length + " " + length


class Classifier(object):

    def classify(self, X_to_test):
        """
        Recieves a list of m unclassified headlines, and predicts for each one which newspaper published it.
        :param X: A list of length m containing the headlines' texts (strings)
        :return: y_hat - a binary vector of length m
        """

        if TRAIN_CLASSIFIER and os.path.exists("haaretz.csv") and os.path.exists("israelhayom.csv") and os.path.isfile("haaretz.csv") and os.path.isfile("israelhayom.csv"):
            import load_headlines
            X, y = load_headlines.load_dataset()

            classifier = Pipeline([('vect', CountVectorizer(ngram_range=(1, 7), analyzer='char', preprocessor=pre_process)),
                                   ('trans', TfidfTransformer()),
                                   ('clf', SGDClassifier(alpha=0.0001))
                                   ])

            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=33)

            classifier.fit(X_train, y_train)

            if SAVE_PICKLE:
                with open('babies_classifier.pkl', 'wb') as fid:
                    pickle.dump(classifier, fid)

        else:
            with open('babies_classifier.pkl', 'rb') as fid:
                classifier = pickle.load(fid)

        return classifier.predict(X_to_test)
