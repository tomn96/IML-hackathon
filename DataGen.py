"""
File: DataGen.py
Author: The Babies TM
Brief: Generates Data
"""


from load_headlines import *


HAARETZ_LABEL = 0
ISREAL_LABEL = 1
STOP_WORDS = [u'', u'a', u'about', u'above', u'after', u'again', u'against',
              u'all', u'am', u'an', u'and', u'any', u'are', u"aren't", u'as',
              u'at', u'be', u'because', u'been', u'before', u'being', u'below',
              u'between', u'both', u'but', u'by', u"can't", u'cannot',
              u'could', u"couldn't", u'did', u"didn't", u'do', u'does',
              u"doesn't", u'doing', u"don't", u'down', u'during', u'each',
              u'few', u'for', u'from', u'further', u'had', u"hadn't", u'has',
              u"hasn't", u'have', u"haven't", u'having', u'he', u"he'd",
              u"he'll", u"he's", u'her', u'here', u"here's", u'hers',
              u'herself', u'him', u'himself', u'his', u'how', u"how's",
              u'i', u"i'd", u"i'll", u"i'm", u"i've", u'if', u'in', u'into',
              u'is', u"isn't", u'it', u"it's", u'its', u'itself', u"let's",
              u'me', u'more', u'most', u"mustn't", u'my', u'myself', u'no',
              u'nor', u'not', u'of', u'off', u'on', u'once', u'only', u'or',
              u'other', u'ought', u'our', u'ours\tourselves', u'out', u'over',
              u'own', u'same', u"shan't", u'she', u"she'd", u"she'll",
              u"she's", u'should', u"shouldn't", u'so', u'some', u'such',
              u'than', u'that', u"that's", u'the', u'their', u'theirs',
              u'them', u'themselves', u'then', u'there', u"there's",
              u'these', u'they', u"they'd", u"they'll", u"they're",
              u"they've", u'this', u'those', u'through', u'to', u'too',
              u'under', u'until', u'up', u'very', u'was', u"wasn't", u'we',
              u"we'd", u"we'll", u"we're", u"we've", u'were', u"weren't",
              u'what', u"what's", u'when', u"when's", u'where', u"where's",
              u'which', u'while', u'who', u"who's", u'whom', u'why', u"why's",
              u'with', u"won't", u'would', u"wouldn't", u'you', u"you'd",
              u"you'll", u"you're", u"you've", u'your', u'yours', u'yourself',
              u'yourselves']


class DataGen:
    """ Generates a data representation for the classifier. """

    @staticmethod
    def generate_full():
        """ Generate a dictionary with the headline and it's
        corresponding label. """
        X, y = load_dataset()
        return {headline: label for headline, label in zip(X, y)}

    @staticmethod
    def generate_partial():
        """ Generate a dictionary with the headline and it's
        corresponding label. The headline is modified and all the
        prepositions have been removed. """
        X, y = load_dataset()
        data = dict()  # The final result.
        # For each headline, remove it's stop words and add to the result.
        for i in range(len(X)):
            # Split the headline into words.
            parse_headline = X[i].split()
            update_headline = list()
            for word in parse_headline:
                if word not in STOP_WORDS:
                    # If the current word in this headline is not a stop word.
                    update_headline.append(word)
            # Create an updated string without the stop words.
            update_headline = ' '.join(update_headline)
            data[update_headline] = y[i]
        return data